<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->string('number', 20);
            $table->decimal('total_price', 10, 2);
            $table->enum('payment_status', [1, 2, 3, 4])->comment('1=menunggu pembayaran, 2=sudah dibayar, 3=kadaluarsa, 4=batal')->default(1);
            $table->string('snap_token', 36)->nullable();
            $table->string('payment_link_id', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
