const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps();
// mix.copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/font');
mix.styles([
    'resources/css/style.css',
    'resources/css/animate.css',
    'resources/css/plugins/textSpinners/spinners.css',
    'resources/css/plugins/gritter/jquery.gritter.min.css',
    'resources/css/plugins/select2/select2.min.css',
    'resources/css/plugins/select2/select2.bootstrap.min.css',
], 'public/css/main.css').version();
mix.scripts([
    'resources/js/popper.min.js',
    'resources/js/library.js',
    'resources/js/inspinia.js',
    'resources/js/plugins/pace/pace.min.js',
    'resources/js/plugins/metisMenu/jquery.metisMenu.js',
    'resources/js/plugins/slimscroll/jquery.slimscroll.min.js',
    'resources/js/plugins/blockui/jquery.blockUI.js',
    'resources/js/plugins/gritter/jquery.gritter.min.js',
    'resources/js/plugins/select2/select2.min.js',
    'resources/js/plugins/inputmask/jquery.inputmask.js',
    'resources/js/plugins/moment/moment.min.js',
], 'public/js/main.js').version();