@extends('layouts.bootstrap')

@section('content')
<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-4 col-lg-10 mx-auto">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg">
                    <div class="p-5">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="h4 text-gray-900 mb-4">Order <button class="btn btn-danger">{{ $order->number }}</button></h1>
                            </div>
                            <div class="col-md-6 text-center">
                            </div>
                            <hr>
                        </div>

                        <div class="row">
                            <div class="col-md-6">

                                <h2>
                                    <strong>{{ $order->user->name }}</strong>
                                </h2>
                                <br>
                                <div>{{ $order->user->address }}</div>
                                <div>Email: {{ $order->user->email }}</div>
                                <div>Phone: {{ $order->user->phone }}</div>
                                <div>Category: {{ $order->user->category }}</div>
                                <div>Bill: <strong>Rp. 100.000</strong> </div>
        
                                <!-- {{ json_encode(session()->all()) }} -->
                                @if ($order->payment_status == 1)
                                <br><br>
                                @else
                                Pembayaran berhasil
                                @endif
                                <br><br>
                            </div>
                            <div class="col-md-6 text-center">
                                <label class="small mb-1" for="info">Harap Lakukan Pembayaran ke rek tercantum.</label>
                                <br>
                                <button class="btn btn-primary" id="pay-button"><h2>XXXXXXXXX</h2></button>
                                <br>
                                <label class="small mb-1" for="info">Petunjuk pembayaran :</label>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection