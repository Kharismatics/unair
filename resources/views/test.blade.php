@extends('layouts.bootstrap')
<style>
    div.radio-with-Icon {
        display: block;
    }

    div.radio-with-Icon p.radioOption-Item {
        display: inline-block;
        width: 100px;
        height: 100px;
        box-sizing: border-box;
        margin: 25px 15px;
        border: none;
    }

    div.radio-with-Icon p.radioOption-Item label {
        display: block;
        height: 100%;
        width: 100%;
        padding: 10px;
        border-radius: 10px;
        border: 1px solid #de1831;
        color: #de1831;
        cursor: pointer;
        opacity: .8;
        transition: none;
        font-size: 13px;
        padding-top: 25px;
        text-align: center;
        margin: 0 !important;
    }

    div.radio-with-Icon p.radioOption-Item label:hover,
    div.radio-with-Icon p.radioOption-Item label:focus,
    div.radio-with-Icon p.radioOption-Item label:active {
        opacity: .5;
        background-color: #de1831;
        color: #fff;
        margin: 0 !important;
    }

    div.radio-with-Icon p.radioOption-Item label::after,
    div.radio-with-Icon p.radioOption-Item label:after,
    div.radio-with-Icon p.radioOption-Item label::before,
    div.radio-with-Icon p.radioOption-Item label:before {
        opacity: 0 !important;
        width: 0 !important;
        height: 0 !important;
        margin: 0 !important;
    }

    div.radio-with-Icon p.radioOption-Item label i.fa {
        display: block;
        font-size: 50px;
    }

    div.radio-with-Icon p.radioOption-Item input[type="radio"] {
        opacity: 0 !important;
        width: 0 !important;
        height: 0 !important;
    }

    div.radio-with-Icon p.radioOption-Item input[type="radio"]:active~label {
        opacity: 1;
    }

    div.radio-with-Icon p.radioOption-Item input[type="radio"]:checked~label {
        opacity: 1;
        border: none;
        background-color: #de1831;
        color: #fff;
    }

    div.radio-with-Icon p.radioOption-Item input[type="radio"]:hover,
    div.radio-with-Icon p.radioOption-Item input[type="radio"]:focus,
    div.radio-with-Icon p.radioOption-Item input[type="radio"]:active {
        margin: 0 !important;
    }

    div.radio-with-Icon p.radioOption-Item input[type="radio"]+label:before,
    div.radio-with-Icon p.radioOption-Item input[type="radio"]+label:after {
        margin: 0 !important;
    }
</style>
@section('content')
<div class="container">

    <div class="card o-hidden border-0 shadow-lg col-lg-8 mx-auto">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <!-- @if ($errors->any())
   @foreach ($errors->all() as $error)
      <div>{{$error}}</div>
   @endforeach
@endif -->
                <div class="col-lg">
                    <div class="p-5">
                        <div class="row">
                            <div class="col-md-6">
                                <h1 class="h4 text-gray-900 mb-4">Participant Form</h1>
                            </div>
                            <div class="col-md-6 text-center">
                            </div>
                            <hr>
                        </div>
                        <form class="user" method="post" action="registration">
                            @csrf
                            <div class="form-group">
                                <label>Full Name</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name">

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" placeholder="">

                                @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>NIK</label>
                                <input placeholder="16 digit number" id="nik" type="text" class="form-control @error('nik') is-invalid @enderror" name="nik" value="{{ old('nik') }}" required autocomplete="nik" placeholder="">

                                @error('nik')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <!-- <label for="gender">Gender </label> <br>
                                    <input type="radio" name="gender" value="L" id="genderL" class="@error('gender') is-invalid @enderror"> Male
                                    <input type="radio" name="gender" value="P" id="genderP" class="@error('gender') is-invalid @enderror"> Female
                                </label> -->
                                <!-- <label>Gender</label>
                                <input type="radio" value="Male" id="male" name="gender" class="@error('gender') is-invalid @enderror">
                                <label for="male">Male</label>
                                <input type="radio" value="Female" id="female" name="gender">
                                <label for="female">Female</label> -->
                                <label for="gender">Gender</label><br>
                                <div class="form-check form-check-inline @error('gender') is-invalid @enderror">
                                    <input class="form-check-input" type="radio" name="gender" id="gender1" value="L" required {{ (old('gender') == 'L' ? 'checked' : '') }}>
                                    <label class="form-check-label" for="gender1">Male</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="gender" id="gender2" value="P" required {{ (old('gender') == 'P' ? 'checked' : '') }}>
                                    <label class="form-check-label" for="gender2">Female</label>
                                </div>
                                @error('gender')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Emergency Name</label>
                                <input id="emergency_name" type="text" class="form-control @error('emergency_name') is-invalid @enderror" name="emergency_name" value="{{ old('emergency_name') }}" required autocomplete="emergency_name" placeholder="">

                                @error('emergency_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Emergency Phone</label>
                                <input id="emergency_phone" type="text" class="form-control @error('emergency_phone') is-invalid @enderror" name="emergency_phone" value="{{ old('emergency_phone') }}" required autocomplete="emergency_phone" placeholder="">

                                @error('emergency_phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group text-center">
                                <div class="radio-with-Icon">
                                    <p class="radioOption-Item">
                                        <input type="radio" name="category" id="category1" value="RUN" class="ng-valid ng-dirty ng-touched ng-empty" aria-invalid="false" {{ (old('category') == 'RUN' ? 'checked' : '') }}>
                                        <label for="category1">
                                            <i class="fa fa-run"></i>
                                            Run
                                        </label>
                                    </p>

                                    <p class="radioOption-Item">
                                        <input type="radio" name="category" id="category2" value="RIDE" class="ng-valid ng-dirty ng-touched ng-empty" aria-invalid="false" {{ (old('category') == 'RIDE' ? 'checked' : '') }}>
                                        <label for="category2">
                                            <i class="fa fa-bike"></i>
                                            Ride
                                        </label>
                                    </p>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                Register Account
                            </button>
                            <br><br>
                            <!-- <label class="small mb-1" for="info">Jika Anda belum mendapatkan email verifikasi hubungi admin pada web event.</label> -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        @if (count($errors) == 1 && $errors->get('category'))
            swal("Error !", "Please Select one Run / Ride ", "error");
        @endif
    })
</script>
@endsection