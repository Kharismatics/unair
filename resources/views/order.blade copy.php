@extends('layouts.bootstrap')

@section('content')
<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-4 col-lg-8 mx-auto">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg">
                    <div class="p-5">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="h4 text-gray-900 mb-4">Order {{ $order->number }}</h1>
                            </div>
                            <div class="col-md-6 text-center">
                            </div>
                            <hr>
                        </div>

                        <div>
                            <strong>{{ $order->user->name }}</strong>
                        </div>
                        <br>
                        <div>{{ $order->user->address }}</div>
                        <div>Email: {{ $order->user->email }}</div>
                        <div>Phone: {{ $order->user->phone }}</div>

                        <!-- {{ json_encode(session()->all()) }} -->
                        @if ($order->payment_status == 1)
                        <br><br>
                        <button class="btn btn-primary" id="pay-button">Bayar Sekarang</button>
                        @else
                        Pembayaran berhasil
                        @endif
                        <br><br>
                        <label class="small mb-1" for="info">Jika Anda belum mendapatkan email verifikasi hubungi admin pada web event.</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@php 
    $url_script = config('midtrans.is_production') ? 'https://app.midtrans.com/snap/snap.js' : 'https://app.sandbox.midtrans.com/snap/snap.js';
@endphp
<!-- <input type="hidden" id="snap-token" value="{{ $snapToken }}" class="form-input">
<script type="text/javascript" src="{{ $url_script }}" data-client-key="{{ config('midtrans.client_key') }}"></script>

<script type="text/javascript">
    var payButton = document.getElementById('pay-button');

    /* For example trigger on button clicked, or any time you need */
    payButton.addEventListener('click', function() {
        /* in this case, the snap token is retrieved from the Input Field */
        var snapToken = document.getElementById('snap-token').value;
        snap.pay(snapToken);
    });
</script> -->
<script>
    $(document).ready(function() {

    })
</script>
<script src="{{ $url_script }}" data-client-key="{{ config('midtrans.client_key') }}"></script>
<script>
    const payButton = document.querySelector('#pay-button');
    payButton.addEventListener('click', function(e) {
        e.preventDefault();

        snap.pay('{{ $snapToken }}', {
            // Optional
            onSuccess: function(result) {
                /* You may add your own js here, this is just example */
                // document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
                console.log(result)
            },
            // Optional
            onPending: function(result) {
                /* You may add your own js here, this is just example */
                // document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
                console.log(result)
            },
            // Optional
            onError: function(result) {
                /* You may add your own js here, this is just example */
                // document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
                console.log(result)
            }
        });
    });
</script>
@endsection