<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>

<body>
    @if (Auth::guest())
    <div style="margin-top: 100px"></div>
    <!-- @yield('content') -->
    @else
    <!-- Wrapper-->
    <div id="wrapper">

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <img alt="image" class="rounded-circle" src="img/profile_small.jpg" />
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="block m-t-xs font-bold">David Williams</span>
                                <span class="text-muted text-xs block">Art Director <b class="caret"></b></span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a class="dropdown-item" href="profile.html">Profile</a></li>
                                <li><a class="dropdown-item" href="contacts.html">Contacts</a></li>
                                <li><a class="dropdown-item" href="mailbox.html">Mailbox</a></li>
                                <li class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="login.html">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            IRACE
                        </div>
                    </li>
                    <!-- nav -->
                    @include('layouts.nav')
                    <!-- end nav -->
                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg">
            <!-- top navbar -->
            @include('layouts.topnav')
            <!-- end top navbar -->
            <!-- bread title  -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Title</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.html">Title</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Current Title</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
            <!-- end bread title  -->
            <!-- content -->
            <div class="wrapper wrapper-content  animated fadeInRight">
                @yield('content')
                <!-- <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox ">
                            <div class="ibox-title">
                                <h5>Example Content</h5>
                            </div>
                            <div class="ibox-content">
                                <p>
                                    Example Body
                                </p>
                                <div class="row">
                                    <div class="col-md-4">
                                        <h4>Basic Content</h4>
                                        <a href="email_templates/action.html" target="_blank">
                                            <img src="img/email_1.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-4">
                                        <h4>Basic Content</h4>
                                        <a href="email_templates/alert.html" target="_blank">
                                            <img src="img/email_2.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-4">
                                        <h4>Basic Content</h4>
                                        <a href="email_templates/billing.html" target="_blank">
                                            <img src="img/email_3.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
            <!-- end content -->
            <!-- footer -->
            @include('layouts.footer')
            <!-- end footer -->
        </div>
    </div>
    <!-- end wrapper-->
    @endif
</body>

</html>