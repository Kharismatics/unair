<li>
    <a href="widgets.html"><i class="fa fa-flask"></i> <span class="nav-label">Event</span></a>
</li>
<li>
    <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Activity</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="form_basic.html">Basic form</a></li>
    </ul>
</li>
<li>
    <a href="#"><i class="fa fa-desktop"></i> <span class="nav-label">App Views</span> <span class="float-right label label-primary">SPECIAL</span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="contacts.html">Contacts</a></li>
    </ul>
</li>
<li>
    <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Menu Levels </span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li>
            <a href="#">Third Level <span class="fa arrow"></span></a>
            <ul class="nav nav-third-level">
                <li>
                    <a href="#">Third Level Item</a>
                </li>

            </ul>
        </li>
        <li><a href="#">Second Level Item</a></li>
    </ul>
</li>
<li>
    <a href="css_animation.html"><i class="fa fa-magic"></i> <span class="nav-label">CSS Animations </span><span class="label label-info float-right">62</span></a>
</li>
<li class="landing_link">
    <a target="_blank" href="landing.html"><i class="fa fa-star"></i> <span class="nav-label">Connect</span> <span class="label label-warning float-right">NEW</span></a>
</li>