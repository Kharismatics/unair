<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'number',
        'total_price',
        'payment_status',
        'snap_token',
        'payment_link_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
