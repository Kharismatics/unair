<?php
 
namespace App\Services\Midtrans;
 
use Midtrans\Snap;
 
class SnapService extends Midtrans
{
    protected $order;
 
    public function __construct($order)
    {
        parent::__construct();
 
        $this->order = $order;
    }
 
    public function getSnapToken()
    {
        $params = [
            'transaction_details' => [
                'order_id' => $this->order->number,
                'gross_amount' => $this->order->total_price,
                "payment_link_id"=> "for-payment-".$this->order->number,
            ],
            // 'item_details' => [
            //     [
            //         'id' => 1,
            //         'price' => '150000',
            //         'quantity' => 1,
            //         'name' => 'Flashdisk Toshiba 32GB',
            //     ],
            //     [
            //         'id' => 2,
            //         'price' => '60000',
            //         'quantity' => 2,
            //         'name' => 'Memory Card VGEN 4GB',
            //     ],
            // ],
            'customer_details' => [
                'first_name' => $this->order->user->name,
                'email' => $this->order->user->email,
                'phone' => $this->order->user->phone,
            ],
            // "event" => "Unair 2022",
            // "category" => "Run 5K",
            "custom_field1" => "Unair Run 2022",
            "custom_field2" => "Run 5K",
        ];

 
        $snapToken = Snap::getSnapToken($params);
 
        return $snapToken;
    }
    public function getSnapUrl()
    {
        $params = [
            'transaction_details' => [
                'order_id' => $this->order->number,
                'gross_amount' => $this->order->total_price,
                "payment_link_id"=> "for-payment-".$this->order->number,
            ],
            // 'item_details' => [
            //     [
            //         'id' => 1,
            //         'price' => '150000',
            //         'quantity' => 1,
            //         'name' => 'Flashdisk Toshiba 32GB',
            //     ],
            //     [
            //         'id' => 2,
            //         'price' => '60000',
            //         'quantity' => 2,
            //         'name' => 'Memory Card VGEN 4GB',
            //     ],
            // ],
            'customer_details' => [
                'first_name' => 'ckc',
                'email' => 'kharismatics@gmail.com',
                'phone' => '081234567890',
            ]
        ];

 
        $snapToken = Snap::getSnapUrl($params);
 
        return $snapToken;
    }
}