<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Services\Midtrans\SnapService;
class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::all();
        return $orders;
        // return view('order', compact('orders'));
    }
    // public function show(Request $request,Order $order)
    // {
    //     // return $request->all();
    //     // return $order;
    //     $snapToken = $order->snap_token;
    //     if (empty($snapToken)) {
    //         // Jika snap token masih NULL, buat token snap dan simpan ke database
 
    //         $midtrans = new SnapService($order);
    //         $snapToken = $midtrans->getSnapToken();
 
    //         $order->snap_token = $snapToken;
    //         $order->save();
    //     }
    //     // return $id;
    //     return view('order', compact('order', 'snapToken'));
 
    //     // // return view('orders.show', compact('order', 'snapToken'));
    // }
    public function show($number)
    {
        // dd($number);
        // return $request->all();
        // return $order;
        $order = Order::where('number', $number) ->with('user') ->first();
        // $snapToken = $order->snap_token;
        // if (empty($snapToken)) {
        //     // Jika snap token masih NULL, buat token snap dan simpan ke database
 
        //     $midtrans = new SnapService($order);
        //     $snapToken = $midtrans->getSnapToken();
 
        //     $order->snap_token = $snapToken;
        //     $order->save();
        // }
        // $paymentLink = $order->payment_link_id;
        // if (empty($paymentLink)) {
        //     // Jika snap token masih NULL, buat token snap dan simpan ke database
 
        //     $midtrans = new SnapService($order);
        //     $paymentLink = $midtrans->getSnapUrl();
 
        //     $order->payment_link_id = $paymentLink;
        //     $order->save();
        // }
        // // return $id;
        // // return $order;
        // return view('order', compact('order', 'snapToken'));
        return view('order', compact('order'));
    }
}
