<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Models\Order;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // return view('home');
        return view('test');
    }
    public function register(RegisterRequest $request)
    {
        // dd($request->all());
        // return view('home');
        // return view('test');
        $newUser = User::create($request->all());
        $newUser = $newUser->fresh();
        $checkOrder = Order::where('user_id', $newUser->id)
            ->where('payment_status', 1)
            ->first();
        if ($checkOrder) {
            // // $checkOrder->update([
            // //     'user_id' => $newUser->id,
            // //     'number' => $newUser->id . '-' . date('YmdHis'),
            // //     'total_price' => $checkOrder->total_price,
            // //     'payment_status' => 1,
            // //     'snap_token' => $request->snap_token,
            // // ]);
            // session()->flash('message', __('text.successfully_added'));
            // session()->flash('alert-class', 'alert-success');
            // return redirect('order');
        } else {
            $newOrder = Order::create([
                'user_id' => $newUser->id,
                'number' => str_pad($newUser->id, 5, 0, STR_PAD_LEFT). date('YmdHis'),
                'total_price' => 100000
            ]);
            return redirect()->route('order',$newOrder->number);
            
        }

        // // return redirect()->route('home');
        // session()->flash('message', __('text.successfully_added'));
        // session()->flash('alert-class', 'alert-success');
        // return redirect('order');
    }
}
